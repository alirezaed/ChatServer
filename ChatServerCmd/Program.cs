﻿using System.Configuration;
using System.Net;

namespace ChatServer
{
    class Program
    {
        static void Main(string[] args)
        {
            string IpAddress = ConfigurationManager.AppSettings["IPAddress"];
            int Port = 8081;
            int.TryParse(ConfigurationManager.AppSettings["Port"], out Port);
            Server server = new Server(100, 2048, new Logger(), Authentication.Authentication.GetInstance());
            server.Start(new IPEndPoint(IPAddress.Parse(IpAddress), Port));
        }
    }
}
