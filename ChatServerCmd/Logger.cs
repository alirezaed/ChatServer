﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatServer.Interface;
namespace ChatServer
{
    class Logger :ILogger
    {

        public void WritLog(Exception ex)
        {
            Console.WriteLine(ex.Message);
        }

        public void WritLog(string message)
        {
            Console.WriteLine(message);
        }

        public void WritLog(string message, params object[] inputs)
        {
            Console.WriteLine(string.Format(message, inputs));
        }
    }
}
