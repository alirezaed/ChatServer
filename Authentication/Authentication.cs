﻿using ChatServer.Interface;
using Authentication.Model;
using Authentication.Redis;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using WebApi;

namespace Authentication
{
    public class Authentication : IAuthentication
    {
        static IDatabase db;
        Redis.RedisObjectStore<User> user;
        static Authentication _authentication;
        private JavaScriptSerializer jss = new JavaScriptSerializer();
        private Authentication()
        {
            db = RedisConnectionFactory.GetConnection().GetDatabase();
            user = new RedisObjectStore<User>(db);
        }

        public static Authentication GetInstance()
        {
            if (_authentication == null)
                _authentication = new Authentication();
            return _authentication;
        }
        public bool UserExists(string phoneNo)
        {
            try
            {
                return db.KeyExists(user.ChatPrefix + phoneNo + ":authentication.model.user") && user.GetUser(phoneNo).Reference != Guid.Empty.ToString();
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public bool IsPasswordCorrect(string phoneNo, string password)
        {
            return user.GetUser(phoneNo).Password == password;
        }

        public bool IsSecurityCodeCorrect(string phoneNo, string securityCode)
        {
            return user.GetUser(phoneNo).SecurityCode == securityCode;
        }

        public bool SaveNewUser(string phoneNo)
        {
            User newUser = new User
            {
                UserName = string.Empty,
                Reference = Guid.Empty.ToString(),
                PhoneNo = phoneNo,
                SecurityCode = "123",
                Password = string.Empty
            };
            user.SaveUser(phoneNo, newUser);
            return true;
        }

        public bool UpdateUserInfo(string phoneNo, string password, string username)
        {
            User updateUser = new User
            {
                PhoneNo = phoneNo,
                Password = password,
                UserName = username,
                SecurityCode = "",
                Reference = Guid.NewGuid().ToString()
            };
            user.SaveUser(phoneNo, updateUser);
            return true;
        }

        public string GetUser(string phoneNo)
        {
            User userobj = user.GetUser(phoneNo);
            return string.Format("username:{0}:reference:{1}", userobj.UserName, userobj.Reference);
        }
        public string GenerateToken(string phoneNo)
        {
            string token = JwtManager.GenerateToken(phoneNo);
            db.StringSet("tokens:" + token, phoneNo);
            return token;
        }

        public bool IsAuthenticated(string token)
        {
            return JwtManager.GetPrincipal(token).Identity.IsAuthenticated;
        }
        public string GetPhoneNo(string token)
        {
            return db.StringGet("tokens:" + token);
        }
    }
}
