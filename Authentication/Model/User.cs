﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Authentication.Model
{
    public class User
    {
        public string PhoneNo { get; set; }
        public string Reference { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string SecurityCode { get; set; }
    }
}
