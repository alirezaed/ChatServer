﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
                  name: "UserExists",
                  routeTemplate: "api/authentication/UserExists/{phoneNo}",
                  defaults: new { controller = "Authentication", action = "UserExists" }
                );
            config.Routes.MapHttpRoute(
                 name: "SaveNewUser",
                 routeTemplate: "api/authentication/SaveNewUser/{phoneNo}",
                 defaults: new { controller = "Authentication", action = "SaveNewUser" }
               );
            config.Routes.MapHttpRoute(
                 name: "IsPasswordCorrect",
                 routeTemplate: "api/authentication/IsPasswordCorrect/{phoneNo}/{password}",
                 defaults: new { controller = "Authentication", action = "IsPasswordCorrect" }
               );
            config.Routes.MapHttpRoute(
                 name: "IsSecurityCodeCorrect",
                 routeTemplate: "api/authentication/IsSecurityCodeCorrect/{phoneNo}/{securityCode}",
                 defaults: new { controller = "Authentication", action = "IsSecurityCodeCorrect" }
               );
            config.Routes.MapHttpRoute(
                 name: "UpdateUserInfo",
                 routeTemplate: "api/authentication/UpdateUserInfo/{phoneNo}/{password}/{username}",
                 defaults: new { controller = "Authentication", action = "UpdateUserInfo" }
               );
            config.Routes.MapHttpRoute(
                 name: "GetUser",
                 routeTemplate: "api/authentication/GetUser/{phoneNo}",
                 defaults: new { controller = "Authentication", action = "GetUser" }
               );
            config.Routes.MapHttpRoute(
                 name: "GenerateToken",
                 routeTemplate: "api/authentication/GenerateToken/{phoneNo}",
                 defaults: new { controller = "Authentication", action = "GenerateToken" }
               );
            config.Routes.MapHttpRoute(
                 name: "IsAuthenticated",
                 routeTemplate: "api/authentication/IsAuthenticated/{token}",
                 defaults: new { controller = "Authentication", action = "IsAuthenticated" }
               );
        }
    }
}
