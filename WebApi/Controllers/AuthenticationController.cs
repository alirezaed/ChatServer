﻿using ChatServer.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.Controllers
{
    public class AuthenticationController : ApiController
    {
        IAuthentication authentication = Authentication.Authentication.GetInstance();
        [HttpGet]
        public bool UserExists(string phoneNo)
        {
            return authentication.UserExists(phoneNo);
        }
        [HttpGet]
        public bool IsPasswordCorrect(string phoneNo,string password)
        {
            return authentication.IsPasswordCorrect(phoneNo, password);
        }
        [HttpGet]
        public bool IsSecurityCodeCorrect(string phoneNo, string securityCode)
        {
            return authentication.IsSecurityCodeCorrect(phoneNo, securityCode);
        }
        [HttpGet]
        public bool SaveNewUser(string phoneNo)
        {
            return authentication.SaveNewUser(phoneNo);
        }
        [HttpGet]
        public bool UpdateUserInfo(string phoneNo, string password, string username)
        {
            return authentication.UpdateUserInfo(phoneNo, password, username);
        }
        [HttpGet]
        public string GetUser(string phoneNo)
        {
            return authentication.GetUser(phoneNo);
        }
        [HttpGet]
        public string GenerateToken(string phoneNo)
        {
            return authentication.GenerateToken(phoneNo);
        }
        [HttpGet]
        public bool IsAuthenticated(string token)
        {
            return authentication.IsAuthenticated(token);
        }
    }
}
