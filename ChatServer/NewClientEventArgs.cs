﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatServer
{
    public class NewClientEventArgs : EventArgs
    {
        private string joinedUserName;
        private string allActiveUsers;
        private string simpleToken;

        public string JoinedUserName{ get { return joinedUserName; } set { joinedUserName = value; } }
        public string AllActiveUsers { get { return allActiveUsers; } set { allActiveUsers = value; } }
        public string SimpleToken
        {
            get { return simpleToken; }
            set { this.simpleToken = value; }
        }
    }
}
