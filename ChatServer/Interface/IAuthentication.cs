﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ChatServer.Interface
{
    public interface IAuthentication
    {
        bool UserExists(string phoneNo);

        bool IsPasswordCorrect(string phoneNo, string password);

        bool IsSecurityCodeCorrect(string phoneNo, string securityCode);

        bool SaveNewUser(string phoneNo);

        bool UpdateUserInfo(string phoneNo, string password, string username);

        string GetUser(string phoneNo);

        string GenerateToken(string phoneNo);
        bool IsAuthenticated(string token);

        string GetPhoneNo(string token);

    }
}
