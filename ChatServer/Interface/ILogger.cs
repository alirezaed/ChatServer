﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatServer.Interface
{
    public interface ILogger
    {
        void WritLog(string message);
        void WritLog(string message,params object[] inputs);
        void WritLog(Exception ex);
    }
}
