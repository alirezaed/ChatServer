﻿using ChatServer.Interface;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Script.Serialization;

namespace ChatServer
{
    public class Server : SocketServer
    {
        public Server(int numConnections, int receiveBufferSize, ILogger logger, IAuthentication authentication) : base(numConnections, receiveBufferSize, logger, authentication)
        {
            this.NewMessage += new EventHandler<NewMessageEventArgs>(OnNewMessage);
            this.NewClient += new EventHandler<NewClientEventArgs>(OnNewClientReceived);
        }

        void OnNewMessage(object sender, NewMessageEventArgs e)
        {
            Message msg = new Message
            {
                userName = e.UserName,
                message = e.Message,
                type = MessageType.Message,
                time = DateTime.Now.ToLongTimeString()
            };
            
            SendToAllUser(jss.Serialize(msg));
            //SendToUser("alirezaed", e.Message);
            //SendToClient(e.SimpleToken, "sampledata");
        }

        void OnNewClientReceived(object sender, NewClientEventArgs e)
        {
            /*Send current username and all active users to client*/
            Message msgCurrentUserName = new Message
            {
                type = MessageType.Join,
                message = e.AllActiveUsers,
                userName = e.JoinedUserName
            };
            SendToClient(e.SimpleToken, jss.Serialize(msgCurrentUserName));

            /*Send joined user info to all clients*/
            Message msg = new Message
            {
                type = MessageType.JoinBroadCast,
                message = e.JoinedUserName + " JOINED!",
                userName = e.JoinedUserName
            };
            SendToAllUser(jss.Serialize(msg));
        }
    }

}
