﻿using ChatServer.Interface;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ChatServer
{
    public delegate void NewMessageEventHandler(object sender, NewMessageEventArgs e);
    public delegate void NewClientEventHandler(object sender, NewClientEventArgs e);
    public abstract class SocketServer
    {
        private int m_numConnections;
        private int m_receiveBufferSize;
        BufferManager m_bufferManager;
        const int opsToPreAlloc = 2;
        Socket listenSocket;

        internal SocketAsyncEventArgsPool m_readWritePool;
        internal int m_totalBytesRead;
        internal int m_numConnectedSockets;
        internal Semaphore m_maxNumberAcceptedClients;

        internal JavaScriptSerializer jss = new JavaScriptSerializer();
        internal ConcurrentDictionary<string, Socket> _activeClients; /*key: simpleToken*/
        internal ConcurrentDictionary<string, List<string>> _activeUsers; /*key: username value:simpleTokens*/
        internal ConcurrentDictionary<string, List<string>> _activeChannels; /*key: channelName value:username*/

        internal SHA1 encryptor;
        internal ILogger _logger;
        internal IAuthentication _authentication;
        public event EventHandler<NewMessageEventArgs> NewMessage;
        public event EventHandler<NewClientEventArgs> NewClient;


        public SocketServer(int numConnections, int receiveBufferSize, ILogger logger, IAuthentication authentication)
        {
            m_totalBytesRead = 0;
            m_numConnectedSockets = 0;
            m_numConnections = numConnections;
            m_receiveBufferSize = receiveBufferSize;
            m_bufferManager = new BufferManager(receiveBufferSize * numConnections * opsToPreAlloc, receiveBufferSize);
            _activeClients = new ConcurrentDictionary<string, Socket>();
            _activeUsers = new ConcurrentDictionary<string, List<string>>();
            encryptor = SHA1.Create();
            _logger = logger;
            _authentication = authentication;
            m_readWritePool = new SocketAsyncEventArgsPool(numConnections);
            m_maxNumberAcceptedClients = new Semaphore(numConnections, numConnections);
        }

        public void Init()
        {
            m_bufferManager.InitBuffer();

            SocketAsyncEventArgs readWriteEventArg;


            for (int i = 0; i < m_numConnections; i++)
            {
                readWriteEventArg = new SocketAsyncEventArgs();
                readWriteEventArg.Completed += new EventHandler<SocketAsyncEventArgs>(IO_Completed);
                readWriteEventArg.UserToken = new AsyncUserToken();

                m_bufferManager.SetBuffer(readWriteEventArg);
                m_readWritePool.Push(readWriteEventArg);
            }
        }

        public void Start(IPEndPoint localEndPoint)
        {
            listenSocket = new Socket(localEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            listenSocket.Bind(localEndPoint);
            listenSocket.Listen(100);
            Init();
            Thread tr = new Thread(new ThreadStart(CloseDiedConnections));
            tr.Start();
            _logger.WritLog("Press any key to terminate the server process....");
            while (true)
            {
                StartAccept(null);
            }
        }

        public void StartAccept(SocketAsyncEventArgs acceptEventArg)
        {
            if (acceptEventArg == null)
            {
                acceptEventArg = new SocketAsyncEventArgs();
                acceptEventArg.Completed += new EventHandler<SocketAsyncEventArgs>(AcceptEventArg_Completed);
            }
            else
            {
                acceptEventArg.AcceptSocket = null;
            }

            m_maxNumberAcceptedClients.WaitOne();
            bool willRaiseEvent = listenSocket.AcceptAsync(acceptEventArg);
            if (!willRaiseEvent)
            {
                ProcessAccept(acceptEventArg);
            }
        }

        void AcceptEventArg_Completed(object sender, SocketAsyncEventArgs e)
        {
            ProcessAccept(e);
        }

        private void ProcessAccept(SocketAsyncEventArgs e)
        {
            Interlocked.Increment(ref m_numConnectedSockets);
            _logger.WritLog("Client connection accepted. There are {0} clients connected to the server",
                m_numConnectedSockets);

            SocketAsyncEventArgs readEventArgs = m_readWritePool.Pop();
            ((AsyncUserToken)readEventArgs.UserToken).Socket = e.AcceptSocket;

            bool willRaiseEvent = e.AcceptSocket.ReceiveAsync(readEventArgs);
            if (!willRaiseEvent)
            {
                ProcessReceive(readEventArgs);
            }

            StartAccept(e);
        }



        public void ProcessReceive(SocketAsyncEventArgs e)
        {
            // check if the remote host closed the connection
            AsyncUserToken token = (AsyncUserToken)e.UserToken;
            if (e.BytesTransferred > 0 && e.SocketError == SocketError.Success)
            {
                //increment the count of the total bytes receive by the server
                Interlocked.Add(ref m_totalBytesRead, e.BytesTransferred);
                _logger.WritLog("The server has read a total of {0} bytes", m_totalBytesRead);
                string receivedData = Encoding.UTF8.GetString(e.Buffer.SubArray(e.Offset, e.BytesTransferred));
                if (new Regex("^GET").IsMatch(receivedData))
                {
                    string connectionToken = getParameters(receivedData)["token"];
                    Byte[] response = Encoding.UTF8.GetBytes("HTTP/1.1 101 Switching Protocols" + Environment.NewLine
                        + "Connection: Upgrade" + Environment.NewLine
                        + "Upgrade: websocket" + Environment.NewLine
                        + "Sec-WebSocket-Accept: " + Convert.ToBase64String(
                           encryptor.ComputeHash(
                                Encoding.UTF8.GetBytes(
                                    new Regex("Sec-WebSocket-Key: (.*)").Match(receivedData).Groups[1].Value.Trim() + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
                                )
                            )
                        ) + Environment.NewLine
                        + Environment.NewLine);
                    try
                    {
                        if (_authentication.IsAuthenticated(connectionToken))
                        {
                            ((AsyncUserToken)e.UserToken).ConnectionToken = connectionToken;
                            string phoneNo = _authentication.GetPhoneNo(connectionToken);
                            string userInfo = _authentication.GetUser(phoneNo);
                            string username = userInfo.Split(':')[1];
                            _activeClients.TryAdd(getSimpleToken(connectionToken), token.Socket);
                            addClient(username, getSimpleToken(connectionToken));
                            ((AsyncUserToken)e.UserToken).UserName = username;
                        }
                        else
                        {
                            _logger.WritLog("Invalid Token");
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.WritLog(ex);
                    }
                    /*Accepted*/
                    token.Socket.Send(response);

                    NewClient(this, new NewClientEventArgs
                    {
                        AllActiveUsers = string.Join(",", _activeUsers.Keys),
                        JoinedUserName = ((AsyncUserToken)e.UserToken).UserName,
                        SimpleToken = getSimpleToken(((AsyncUserToken)e.UserToken).ConnectionToken)
                    });

                }
                else
                {
                    string data = e.Buffer.SubArray(e.Offset, e.BytesTransferred).DecodeMessage();
                    if (string.IsNullOrEmpty(data)) return;
                    NewMessage(this, new NewMessageEventArgs
                    {
                        UserName = ((AsyncUserToken)e.UserToken).UserName,
                        Message = data,
                        SimpleToken = getSimpleToken(((AsyncUserToken)e.UserToken).ConnectionToken)
                    });
                }
                if (!token.Socket.Connected) return;
                bool willRaiseEvent = token.Socket.ReceiveAsync(e);
                if (!willRaiseEvent)
                {
                    ProcessReceive(e);
                }
            }
            else
            {
                CloseClientSocket(e);
            }
        }

        public void ProcessSend(SocketAsyncEventArgs e)
        {
            if (e.SocketError == SocketError.Success)
            {
                // done echoing data back to the client
                AsyncUserToken token = (AsyncUserToken)e.UserToken;
                // read the next block of data send from the client
                bool willRaiseEvent = token.Socket.ReceiveAsync(e);
                if (!willRaiseEvent)
                {
                    ProcessReceive(e);
                }
            }
            else
            {
                CloseClientSocket(e);
            }
        }

        private void CloseClientSocket(SocketAsyncEventArgs e)
        {
            AsyncUserToken token = e.UserToken as AsyncUserToken;

            try
            {
                token.Socket.Shutdown(SocketShutdown.Send);
            }
            catch (Exception) { }
            token.Socket.Close();

            Interlocked.Decrement(ref m_numConnectedSockets);
            m_maxNumberAcceptedClients.Release();
            _logger.WritLog("A client has been disconnected from the server. There are {0} clients connected to the server", m_numConnectedSockets);

            m_readWritePool.Push(e);
        }

        void IO_Completed(object sender, SocketAsyncEventArgs e)
        {
            switch (e.LastOperation)
            {
                case SocketAsyncOperation.Receive:
                    ProcessReceive(e);
                    break;
                case SocketAsyncOperation.Send:
                    ProcessSend(e);
                    break;
                default:
                    throw new ArgumentException("The last operation completed on the socket was not a receive or send");
            }

        }

        private Dictionary<string, string> getParameters(string data)
        {
            int start = data.IndexOf("?", 0) + 1;
            int end = data.IndexOf("HTTP", start);

            if (!data.Contains("=")) return new Dictionary<string, string>();

            string[] values = data.Substring(start, end - start).Replace(" ", "").Split('=');
            Dictionary<string, string> result = new Dictionary<string, string>();
            for (int i = 0; i <= values.Length / 2; i++)
                result.Add(values[i++], values[i].Replace("&quot;", ""));
            return result;
        }

        private void BroadcastMessage(object data)
        {
            BroadcastMessageStructure dataIn = (BroadcastMessageStructure)data;
            if (dataIn.toUserName == string.Empty)
            {
                foreach (Socket socket in _activeClients.Values)
                    if (socket.Connected)
                        socket.Send(dataIn.message);
            }
            else
            {
                if (_activeUsers.ContainsKey(dataIn.toUserName))
                {
                    foreach (string simpleToken in _activeUsers[dataIn.toUserName])
                        if (_activeClients.ContainsKey(simpleToken))
                        {
                            _activeClients[simpleToken].Send(dataIn.message);
                        }
                }

            }
        }

        bool SocketConnected(Socket s)
        {
            try
            {
                bool part1 = s.Poll(1000, SelectMode.SelectRead);
                bool part2 = (s.Available == 0);
                if (part1 && part2)
                    return false;
                else
                    return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void CloseDiedConnections()
        {
            while (true)
            {
                foreach (KeyValuePair<string, Socket> item in _activeClients)
                {
                    if (!SocketConnected(item.Value))
                    {
                        if (item.Value.Connected)
                        {
                            item.Value.Shutdown(SocketShutdown.Send);
                            item.Value.Close();
                        }
                        Socket closed = new Socket(SocketType.Stream, ProtocolType.Tcp);
                        _activeClients.TryRemove(item.Key,out closed);
                        removeClient(item.Key);
                        _logger.WritLog("A client has been disconnected from the server. There are {0} clients connected to the server", m_numConnectedSockets);
                    }
                }

                foreach (string user in _activeUsers.Keys) {
                    if (_activeUsers[user].Count == 0)
                    {
                        List<string> removed = new List<string>();
                        _activeUsers.TryRemove(user, out removed);
                    }
                }
                Thread.Sleep(20000);
            }
        }

        private void addClient(string username, string simpleToken)
        {
            if (_activeUsers.ContainsKey(username))
            {
                if (!_activeUsers[username].Contains(simpleToken))
                    _activeUsers[username].Add(simpleToken);
            }
            else
            {
                _activeUsers.TryAdd(username, new List<string>(new string[] { simpleToken }));
            }
        }
        private void removeClient(string simpleToken)
        {
            foreach (string user in _activeUsers.Keys) {
                if (_activeUsers[user].Contains(simpleToken))
                {
                    _activeUsers[user].Remove(simpleToken);
                    return;
                }
                if (_activeUsers[user].Count == 0)
                {
                    List<string> removed = new List<string>();
                    _activeUsers.TryRemove(user, out removed);
                }
            }
        }

        public void SendToUser(string username, string message)
        {
            Thread broadcastThread = new Thread(new ParameterizedThreadStart(BroadcastMessage));
            broadcastThread.Start(new BroadcastMessageStructure { message = message.EncodeMessageToSend(), toUserName = username });
        }

        public void SendToAllUser(string message)
        {
            Thread broadcastThread = new Thread(new ParameterizedThreadStart(BroadcastMessage));
            broadcastThread.Start(new BroadcastMessageStructure { message = message.EncodeMessageToSend(), toUserName = string.Empty });
        }

        public void SendToClient(string clientToken, string message)
        {
            _activeClients[clientToken].Send(message.EncodeMessageToSend());
        }

        public void SendToChannel(string message,string channelName)
        {
            foreach (string username in _activeChannels[channelName])
                SendToUser(username, message);
        }
        private string getSimpleToken(string connectionToken)
        {
            return connectionToken.Substring(connectionToken.Length - 10);
        }
    }
}