﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ChatServer
{
    public class AsyncUserToken
    {
        public string ConnectionToken { get; set; }
        public string UserName { get; set; }
        public Socket Socket { get; set; }
    }
}
