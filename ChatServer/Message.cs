﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatServer
{
    public class Message
    {
        public MessageType type;
        public string message;
        public string userName;
        public string time;
    }
}
