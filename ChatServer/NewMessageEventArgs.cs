﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatServer
{
    public class NewMessageEventArgs : EventArgs
    {
        private string message;
        private string username;
        private string simpleToken;
        public string Message
        {
            get { return message; }
            set { this.message = value; }
        }
        public string UserName
        {
            get { return username; }
            set { this.username = value; }
        }
        public string SimpleToken
        {
            get { return simpleToken; }
            set { this.simpleToken = value; }
        }

    }
}
