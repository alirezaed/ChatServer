﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatServer
{
    public enum MessageType
    {
        JoinBroadCast,
		Message,
		Exit,
        Join
    }
}
